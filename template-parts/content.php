<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hypno-b2b
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('col-ressources-post'); ?>>
	<a href="<?php the_permalink(); ?>">
		<div class="entry-content">
			<?php 
			// the post thumbnail
			echo '<div class="content-post-thumbnail anim-500">' . get_the_post_thumbnail($post->ID) . '</div>';
	

			// the author
			$get_author_id = get_the_author_meta('ID');
			$get_author_gravatar = get_avatar_url($get_author_id, array('size' => 25));

			echo '<div class="d-flex post-author-row"><div class="d-flex align-items-center">' . '<img src="' . $get_author_gravatar . '" alt="'. get_the_title() . '" />' . '<span class="content-post-author">' . get_the_author() . '</span></div>';
	
			// the post category
			$category_detail = get_the_category($post->ID);
			
			foreach($category_detail as $cd){
				echo '<span class="content-post-cat">' . $cd->cat_name . '</span>';
			}
			echo '</div>';
			// the post title
			echo '<h3>'. get_the_title() . '</h3>'; 
	
			// the excerpt
			$excerpt = get_the_excerpt();
			 
			$excerpt = substr($excerpt, 0, 100);
			$result = substr($excerpt, 0, strrpos($excerpt, ' '));
			echo '<p>' . $result . '.. </p>';
	
	
			?>
			<span class="read-more-span"><?php echo __('Lire l\'article','hypno_b2b'); ?></span>
		</div>
	</a>


</article>