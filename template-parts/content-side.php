<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hypno-b2b
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('col-ressources-side'); ?>>
	<a href="<?php the_permalink(); ?>">
		<div class="entry-content post-content-side">
            <div class="post-content-side-img">
                <?php 
                // the post thumbnail
                echo '<div class="content-post-thumbnail anim-500">' . get_the_post_thumbnail($post->ID) . '</div>';
                ?>
            </div>
            <div>
                <?php
                // the post category
                $category_detail = get_the_category($post->ID);
                
                foreach($category_detail as $cd){
                    echo '<span class="content-post-cat">' . $cd->cat_name . '</span>';
                }
                // the post title
                echo '<h3>'. get_the_title() . '</h3>'; 
        
                ?>
                <span class="read-more-span"><?php echo __('Lire l\'article','hypno_b2b'); ?></span>
            </div>
		</div>
	</a>


</article>