<?php

/**
 * Image banner block template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'banner-ressources-' . $block['id'];

if (!empty($block['anchor'])) {
    $id = $block['anchor'];
} ?>
<section class="banner-ressources <?php echo array_key_exists('className', $block) ? $block['className'] : ''; ?>">
    <div class="container-fluid" style="background-color: <?php the_field('bkg_color_full_width'); ?>; transform: translateY(<?php the_field('translate'); ?>%)">
        <div class="container">
            <div class="row">
                <div class="col-12 block-banner-content" style="background-color: <?php the_field('bkg_color'); ?>; transform: translateY(-<?php the_field('translate'); ?>%);">
                    <div>
                        <?php if ( get_field('icon_logo') ) : $image = get_field('icon_logo'); ?>
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                        <?php endif; ?>
                        
                        <?php the_field('content_banner'); ?>
                    </div>
                    <div>
                        <?php
                            if( get_field('modal_type') == 'modal_demo' ) {
                            ?>      
                                <button id="one" class="btn-hypno-light button-modal btn-demo">
                                    <?php echo __('Demander une démo', 'hypno_b2b'); ?>
                                </button>
                            <?php
                            } elseif ( get_field('modal_type') == 'modal_contact' ) {
                            ?>
                                <button id="one" class="btn-hypno-light button-modal-contact btn-demo">
                                    <?php echo __('Nous contacter', 'hypno_b2b'); ?>
                                </button>
                            <?php
                            } else {
                                if ( get_field('link') ) : $file = get_field('link'); ?>
                                    <a class="btn-hypno-light" href="<?php echo $file['url']; ?>"><?php echo $file['title']; ?></a>
                                <?php
                                endif;
                            }
                        ?>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>