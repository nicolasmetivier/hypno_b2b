<?php

/**
 * Image banner block template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'home-dashboard-' . $block['id'];

if (!empty($block['anchor'])) {
    $id = $block['anchor'];
} ?>
<section class="home-dashboard <?php echo array_key_exists('className', $block) ? $block['className'] : ''; ?>">
    <div class="container-fluid" style="background: url(<?php the_field('block_bkg'); ?>);">
        <div class="container">
            <div class="row justify-content-center block-content-repeater">
                <?php if ( have_rows('languages') ) : ?>
                    <?php while( have_rows('languages') ) : the_row(); ?>
                
                        <?php if ( get_sub_field('icone_langue') ) : $image = get_sub_field('icone_langue'); ?>
                        
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                        
                        <?php endif; ?>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-6 block-content">
                    <?php the_field('content'); ?>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 block-content-img mx-auto position-relative">
                    <?php include(get_template_directory() . "/template-parts/blocks/svg-anim-img/svg-anim-img.php"); ?>
                </div>
            </div>
        </div>
    </div>
</section>