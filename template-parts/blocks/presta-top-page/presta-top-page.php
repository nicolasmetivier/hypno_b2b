<?php

/**
 * Image banner block template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'presta-top-page-' . $block['id'];

if (!empty($block['anchor'])) {
    $id = $block['anchor'];
} ?>

<section class="presta-top-page <?php echo array_key_exists('className', $block) ? $block['className'] : ''; ?>">
    <div class="container-fluid h-100" style="background-image: url(<?php the_field('bkg'); ?>);">
        <?php
            if( get_field('reverse') ) {
                $row_reverse = 'row-reverse';
            }
            ?>
            <div class="container h-100">
                <div class="row align-items-center h-100 <?php echo $row_reverse; ?>">
                    <div class="col-12 col-md-7 offset-lg-1 col-lg-6 col-content">
                        <div class="block-titles">
                            <?php 
                             if( get_field('title_tag') ) { ?>
                                <span class="title-tag">
                                    <?php echo __('Offres', 'hypno_b2b'); ?>
                                </span>
                            <?php }
                            ?>
                           <h1>
                                <?php the_title(); ?>
                           </h1> 
                           <div class="bloc-subtitle">
                               <?php the_field('subtitle'); ?>
                           </div>
                        </div>
                        <div>
                        <?php 
                        if( get_field('modal_type') == 'modal_demo' ) {
                        ?>      
                            <button id="one" class="btn-hypno-light button-modal btn-demo">
                                <?php echo __('Demander une démo', 'hypno_b2b'); ?>
                            </button>
                        <?php
                        } elseif ( get_field('modal_type') == 'modal_contact' ) {
                        ?>
                            <button id="one" class="btn-hypno-light button-modal-contact btn-demo">
                                <?php echo __('Nous contacter', 'hypno_b2b'); ?>
                            </button>
                        <?php
                        } else {
                            if ( get_field('link') ) : $file = get_field('link'); ?>
                                <a class="btn-hypno-light" href="<?php echo $file['url']; ?>"><?php echo $file['title']; ?></a>
                            <?php
                            endif;
                        }
                        ?>   
                        </div>
                    </div>
                    <div class="col-12 col-md-5 col-lg-5 col-xl-5 col-img">
                        
                        <?php if ( get_field('mockup_top_page') ) : $image = get_field('mockup_top_page'); ?>
                        
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                        
                        <?php endif; ?>
                        
                    </div>
                </div>
            </div>
    </div>
</section>