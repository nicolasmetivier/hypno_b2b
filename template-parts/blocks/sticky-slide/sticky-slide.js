("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {

        var wW = $(window).width();

        $('.sticky-slide').each(function (){



            var that = $(this);

            if(wW > 576){
                // TIMELINE IN OUT
                var timelinelement = that.find('.sticky-slide-timeline');
                var tlscrollbar = new TimelineMax({paused: true});
                tlscrollbar.to(timelinelement, 0.3, {opacity: 1}, 'start');


                // Part 1
                ScrollTrigger.create({
                    trigger: that,
                    start: 'top top',
                    end: 'bottom bottom',
                    //scrub: true,
                    animation: tlscrollbar,
                    toggleActions: "play reverse play reverse",
                    //markers: true
                });
            }



            // BIG SHAQ ANIM

            let timeline = new TimelineMax();
            if(wW > 576){
                timeline
                .fromTo($('.img-resize .big-shaq'), 0.85, {x: "-50%", width: '200%'}, {width: '100%', x: 0, ease: Power1.easeOut},'first')
                .to($('.sticky-slide-timeline .fixe-timeline .moove-timeline'), 0.9, { height: '100%'},'first')
                .add(() => {tlscrollbar.reverse() ? tlscrollbar.play() : tlscrollbar.reverse();},'first')
                .to($('.img-resize .big-shaq'), 0.15, {opacity : 0},'second')
                .to($('.sticky-slide-timeline .fixe-timeline .moove-timeline'), 0.1, { height: '0%'},'second');
            }

            //if($('.sticky-slide').hasClass('ipr')) timeline.to($('.big-shaq'), {duration: 1, opacity: 0, ease: Power1.easeOut});

            ScrollTrigger.create({
                trigger: $('.scroll-duration'),
                animation: timeline,
                scrub: true,
                start: "top top",
                end: "bottom center",
                //markers: true,
                // onEnterBack: () => {
                //     if($('.sticky-slide').hasClass('ipr')) gsap.to($('.big-shaq'), {duration: 1, opacity: 1, ease: Power1.easeOut});
                // },
                // onLeave: () => {
                //     if($('.sticky-slide').hasClass('ipr')) gsap.to($('.big-shaq'), {duration: 1, opacity: 0, ease: Power1.easeOut});
                // }
            });


            $('.ipr .johnny-cadillac').each(function(i){

                i++;
                let calc = ($(window).height() * 0.15);

                let color;
                let timelineparentcolor;
                let timelinechildcolor;
                switch ($(this)[0].classList[1]) {
                    case 'one':
                        color = "transparent"
                        timelineparentcolor = "#D8D8D8"
                        timelinechildcolor = "#008082"
                        break;
                    case 'two':
                        color = "#058284"
                        timelineparentcolor = "#FFF"
                        timelinechildcolor = "#FFF"
                        break;
                    case 'three':
                        color = "#f3f3f3"
                        timelineparentcolor = "#D8D8D8"
                        timelinechildcolor = "#008082"
                        break;
                
                    default:
                        break;
                }
                
                // ANIM BACKGROUND
                let anim;
                ScrollTrigger.create({
                    trigger: $(this),
                    start: "top center",
                    end: "bottom center",
                    // markers: {startColor: "blue", endColor: "yellow", fontSize: "12px"},
                    //markers: true,
                    onEnter: () => { 
                        clearTimeout(anim);
                        anim = setTimeout(() => {       
                           gsap.to($('.sticky-slide.has_bkg'), {duration: 0.2, backgroundColor: color});
                            if(wW > 576){
                                gsap.to($('.sticky-slide-timeline .fixe-timeline'), {duration:0.2, backgroundColor: timelineparentcolor});
                                gsap.to($('.sticky-slide-timeline .fixe-timeline .moove-timeline'), {duration:0.2, backgroundColor: timelinechildcolor,  height: '0%'});
                            }
                        }, 0);
                    },
                    onEnterBack: () => { 
                        clearTimeout(anim);
                        anim = setTimeout(() => {       
                           gsap.to($('.sticky-slide.has_bkg'), {duration: 0.2, backgroundColor: color});
                            if(wW > 576){
                                gsap.to($('.sticky-slide-timeline .fixe-timeline'), {duration:0.2, backgroundColor: timelineparentcolor});
                                gsap.to($('.sticky-slide-timeline .fixe-timeline .moove-timeline'), {duration:0.2, backgroundColor: timelinechildcolor,  height: '0%'});
                            }
                        }, 0);
                     },
                    onLeave: () => {
                        clearTimeout(anim);
                        anim = setTimeout(() => {       
                           gsap.to($('.sticky-slide.has_bkg'), {duration: 0.2, backgroundColor: color});
                            if(wW > 576){
                                gsap.to($('.sticky-slide-timeline .fixe-timeline'), {duration:0.2, backgroundColor: timelineparentcolor});
                                gsap.to($('.sticky-slide-timeline .fixe-timeline .moove-timeline'), {duration:0.2, backgroundColor: timelinechildcolor,  height: '0%'});
                            }
                        }, 0);
                    },
                    onLeaveBack: () => {
                        clearTimeout(anim);
                        anim = setTimeout(() => {       
                           gsap.to($('.sticky-slide.has_bkg'), {duration: 0.2, backgroundColor: color});
                            if(wW > 576){
                                gsap.to($('.sticky-slide-timeline .fixe-timeline'), {duration:0.2, backgroundColor: timelineparentcolor});
                                gsap.to($('.sticky-slide-timeline .fixe-timeline .moove-timeline'), {duration:0.2, backgroundColor: timelinechildcolor,  height: '0%'});
                            }
                        }, 0);
                    }
                });

                if(wW > 576){
                    // IMAGE ANIM
                    let timeline2 = new TimelineMax();
                    timeline2
                    .to($('#jc-img-'+i), 0.15, {opacity : 1, x: "0vw", ease: Power1.easeInOut})
                    .to($('#jc-img-'+i), 0.70, {opacity : 1, x: "0vw", ease: Power1.easeInOut},'first')
                    .to($('.sticky-slide-timeline .fixe-timeline .moove-timeline'), 0.70, { height: '100%'},'first')
                    .to($('#jc-img-'+i), 0.15, {opacity : 0, x: "5vw", ease: Power1.easeInOut});


                    ScrollTrigger.create({
                        trigger: $(this),
                        animation:timeline2,
                        start: "top center",
                        end: "bottom center",
                        scrub : true
                    });
                

                    // TEXTE ANIM
                    let timeline3 = new TimelineMax();
                    timeline3
                    .to($('#jc-'+i+' .content-sticky-slide'), 1, {y:'25vh', ease: Power1.easeInOut});
                    ScrollTrigger.create({
                        trigger: $(this),
                        animation:timeline3,
                        start: "top center",
                        end: "bottom center",
                        scrub : true,
                        //marker : true,
                    });
                }
                // OPACITY ANIM
                let timeline4 = new TimelineMax();
                timeline4
                .to($('#jc-'+i), 0.25, {opacity : 1, ease: Power1.easeInOut})
                .to($('#jc-'+i), 0.50, {opacity : 1, ease: Power1.easeInOut})
                .to($('#jc-'+i), 0.25, {opacity : 0, ease: Power1.easeInOut});
                ScrollTrigger.create({
                    trigger: $(this),
                    animation:timeline4,
                    start: "top center",
                    end: "bottom center",
                    scrub : true,
                    marker : true,
                });


            });

        });


    });
});
})(jQuery, this);

  