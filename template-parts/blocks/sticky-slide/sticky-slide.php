<?php
    # Custom Block - Sticky Slide

    $version = get_field('version') ? "v2" : "v1"
?>

<section <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"'; ?> class="sticky-slide  <?php if(isset($block['className'])) echo $block['className']; ?> <?php if(get_field('image_per_row')) echo 'ipr'; ?> has_bkg" version="<?= $version ?>" >

    <div class="container">
        <div class="row align-items-end sticky-slide-title">
            <div class="col-12 col-md-6">
                <?php if ( get_field('logo_sticky_slide') ) : $image = get_field('logo_sticky_slide'); ?>
                
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                
                <?php endif; ?>
                <div>
                    <?php the_field('title_sticky_slider'); ?>
                </div>
            </div>
            <div class="col-md-6">
            <?php 
                if( get_field('modal_type') == 'modal_demo' ) {
                ?>      
                    <button id="one" class="btn-hypno button-modal btn-demo ml-auto">
                        <?php echo __('Demander une démo', 'hypno_b2b'); ?>
                    </button>
                <?php
                } elseif ( get_field('modal_type') == 'modal_contact' ) {
                ?>
                    <button id="one" class="btn-hypno button-modal-contact btn-demo ml-auto">
                        <?php echo __('Nous contacter', 'hypno_b2b'); ?>
                    </button>
                <?php
                } else {
                    if ( get_field('link') ) : $file = get_field('link'); ?>
                        <a class="btn-hypno" href="<?php echo $file['url']; ?>"><?php echo $file['title']; ?></a>
                    <?php
                    endif;
                }
                ?> 
            </div>
        </div>
        <div class="row">
            <!-- <div class="col-md-6 ml-auto sticky-slide-left">
                <h2>Nos offres</h2>
            </div> -->
            <div class="col-md-6 ml-auto sticky-slide-left" style="padding-top: 50vh; padding-bottom: 20vh; margin-bottom: 10vh;">
                <?php if(have_rows('content')): ?>
                    <?php while(have_rows('content')): the_row(); ?>
                        <div class="johnny-cadillac <?= get_sub_field('color_class') ?>" id="jc-<?= get_row_index(); ?>" style="height: <?= get_field('r_height'); ?>vh">
                            <div class="content-sticky-slide position-relative">
                                <?php 
                                $image = get_sub_field('titre')['icon'];
                                if( !empty( $image ) ): ?>
                                    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                                <?php endif; ?>
                                <span><?= get_sub_field('titre')['label']; ?></span>
                                <h3><?= get_sub_field('titre')['titre']; ?></h3>
                                <div class="slide-text">
                                    <?= get_sub_field('titre')['text']; ?>
                                </div>
                                <?php 
                                $link = get_sub_field('titre')['bouton'];
                                if( $link ): 
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
                                    <div class="is-style-light-link">    
                                        <a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                                    </div>
                                <?php endif; ?>
                                
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>

            </div>
            <?php if(!my_wp_is_mobile()){ ?>
                <div class="col-md-6 sticky-slide-right p-0">
                    <div class="scroll-height ">
                        <div class="scroll-duration" style="height:<?= get_field('height') + 50; ?>vh"></div>
                        <div class="img-resize">
                            <?= acf_img(get_field('image'), 'large', 'big-shaq') ?>
                            <?php if(have_rows('content')): ?>
                                <?php while(have_rows('content')): the_row(); ?>
                                    <?= acf_img(get_sub_field('image'), 'large', 'jc-img', 'jc-img-'.get_row_index()) ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <?php if(!my_wp_is_mobile()){ ?>
            <div class="sticky-slide-timeline">
                <div class="fixe-timeline">
                    <div class="moove-timeline"></div>
                </div>
            </div>
        <?php } ?>
    </div>

</section>