("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {

        $('.vertical-bar').each(function (i){

          var that = $(this);
          var animed = that.find('.animed');

                    // Part 1
          gsap.to(animed, { ease: Power2.easeOut, scaleY: 1, 
            scrollTrigger: {
              trigger: that,
              start: 'top 60%',
              end: 'bottom 40%',
              scrub: true,
              //markers: true
            } 
          });
    
        })

        
      });
    });
  })(jQuery, this);
