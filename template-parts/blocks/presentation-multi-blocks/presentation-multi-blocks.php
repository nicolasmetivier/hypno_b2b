<?php

/**
 * Image banner block template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'presentation-multi-blocks-' . $block['id'];

if (!empty($block['anchor'])) {
    $id = $block['anchor'];
} ?>

<section class="presentation-multi-blocks <?php echo array_key_exists('className', $block) ? $block['className'] : ''; ?>">
    <div class="container-fluid" style="background: url(<?php the_field('bkg'); ?>);">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div>
                        <span class="title-tag"><?php the_title(); ?></span>
                    </div>
                    <h2>
                        <?php the_field('block_title'); ?>
                    </h2>
                </div>
            </div>
            <div class="row repeater-cards">
                <?php if ( have_rows('repeater_cards') ) : ?>
                    <?php while( have_rows('repeater_cards') ) : the_row(); ?>
                
                        <div class="col-xl-4 col-12 col-sm-6">
                            <div class="card" style="background-color: <?php the_sub_field('bkg_color'); ?>">
                                <!-- OPTIONS FLAGS -->
                                <?php 
                                    if( get_sub_field('flag_list') ) { ?>
                                        <div class="flags-row"> 
                                            <?php if ( have_rows('drapeaux_langues') ) : ?>
                                                <?php while( have_rows('drapeaux_langues') ) : the_row(); ?>

                                                    <?php if ( get_sub_field('flag') ) : $image = get_sub_field('flag'); ?>
                                                    
                                                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>

                                                    <?php endif; ?>
                                                    
                                                <?php endwhile; ?>
                                            <?php endif; ?>
                                        </div>
                                    <?php }
                                    ?>

                                <!-- OPTIONS LIST ITEMS-->
                                <?php 
                                    if( get_sub_field('list_item') ) { ?>
                                        <div class="list-items-card"> 
                                            <?php the_sub_field('list_items'); ?>
                                        </div>
                                     <?php }
                                    ?>
                                <!-- OPTIONS -->
                                <?php the_sub_field('number_key'); ?>
                                <div class="card-content">
                                    <p>
                                        <?php the_sub_field('infos_card'); ?>
                                    </p>
                                </div>
                                <!-- ICONS -->
                                <?php if ( get_sub_field('icon') ) : $image = get_sub_field('icon'); ?>
                                    <div class="icon-cards">
                                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                                    </div>  
                                <?php endif; ?>
                            </div>
                        </div>
                
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>