<?php

/**
 * Image banner block template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'block-form-' . $block['id'];

if (!empty($block['anchor'])) {
    $id = $block['anchor'];
} ?>

<section class="block-form <?php echo array_key_exists('className', $block) ? $block['className'] : ''; ?>">
    <div class="container-fluid h-100" style="background-image: url(<?php the_field('bkg'); ?>); background-repeat: no-repeat; background-size: cover;">
        <?php
            if( get_field('reverse') ) {
                $row_reverse = 'row-reverse';
            }
            ?>
            <div class="container h-100">
                <div class="d-flex flex-column justify-content-end h-100">
                    <div class="row justify-content-between align-items-start <?php echo $row_reverse; ?>">
                        <div class="col-xl-auto col-lg-6 col-12 block-form-form">
                            <div class="col-12 block-form-title-col">
                                <h1>
                                    <?php echo __('Demande de ','hypno_b2b') . '<span>' . get_the_title() . '</span>'; ?>
                                </h1> 
                            </div>
                            <div class="col-12">
                                <?php the_field('subtitle_content'); ?>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-12 block-form-right-col position-relative">
                            <?php if ( get_field('mockup_top_page') ) : $image = get_field('mockup_top_page'); ?>
                                
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                            
                            <?php endif; ?>
                            
                            <div class="">
                                <div class="block-form-right-box-content">
                                    <?php if ( get_field('logo_hypnoledge') ) : $image = get_field('logo_hypnoledge'); ?>
                                        <img class="logo-before" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                                    <?php endif; ?>
                                    <?php the_field('texte_mockup'); ?>
                                </div>
                                <div class="block-form-right-baseline">
                                    <?php the_field('baseline_hypno'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>
<?php 
$url_redirection = get_field('url_redirection');
?>
<script>
    document.addEventListener( 'wpcf7mailsent', function( event ) {
    location = '<?php echo($url_redirection); ?>';
    }, false );
</script>