<?php

/**
 * Image banner block template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'block-2-columns-' . $block['id'];

if (!empty($block['anchor'])) {
    $id = $block['anchor'];
} ?>

<section class="block-2-columns <?php echo array_key_exists('className', $block) ? $block['className'] : ''; ?>">
<?php
    if (get_field('reverse') ) {
        $row_reverse = 'row-reverse';
    }else{
        $row_reverse = '';
    }
    if( get_field('bkg_type') ) { ?>
        <div class="container-fluid" style="background-color: <?php the_field('bkg_color'); ?>">
        <?php 
        } else 
        { ?>
        <div class="container-fluid" style="background: url(<?php the_field('bkg_img'); ?>);">
    <?php
    }
    ?>
        <div class="container">
            <div class="row justify-content-between align-items-center <?php echo $row_reverse; ?>">
                <!----- COL TEXT/CONTENT ----->
                <div class="col-12 col-md-12 col-lg-6 block-content">
                    <div class="d-flex flex-column align-items-center justify-content-center d-lg-flex flex-lg-column align-items-lg-start justify-content-lg-start text-center text-lg-left mb-5 mb-lg-3">
                        <?php if ( get_field('logo') ) : $image = get_field('logo'); ?>
                            <img class="logo-before" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                        <?php endif; ?>

                        <?php 
                        if( get_field('type_page') ) { ?>
                            <span class='page-label'><?php the_title(); ?></span>
                        <?php
                        }
                        ?>
                        
                        <?php the_field('content'); ?>
                    </div>
                    <!-- OPTION LISTE ICONES  -->
                    <?php
                        if (get_field('icons_list') ) { ?>
                            <div class="d-flex flex-wrap">
                                <?php
                                if ( have_rows('list_with_icons') ) : ?>
                                    <?php while( have_rows('list_with_icons') ) : the_row(); ?>
                                    
                                        <div class="row-icon">
                                            <div>
                                                <?php if ( get_sub_field('icon') ) : $image = get_sub_field('icon'); ?>
                                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                                                <?php endif; ?>
                                            </div>
                                            <div class="row-icon-content">
                                                <?php the_sub_field('content'); ?>
                                            </div>
                                        </div>
                                        
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        <?php
                    } 
                    ?>
                    <!-- LIEN BLOCK -->
                    <div class="d-flex flex-column align-items-center justify-content-center d-lg-flex flex-lg-column align-items-lg-start justify-content-lg-start text-center text-lg-left mb-5 mb-lg-3">
                        <?php
                        if( get_field('modal_type') == 'modal_demo' ) {
                        ?>      
                            <button id="one" class="btn-hypno button-modal btn-demo">
                                <?php echo __('Demander une démo', 'hypno_b2b'); ?>
                            </button>
                        <?php
                        } elseif ( get_field('modal_type') == 'modal_contact' ) {
                        ?>
                            <button id="one" class="btn-hypno button-modal-contact btn-demo">
                                <?php echo __('Nous contacter', 'hypno_b2b'); ?>
                            </button>
                        <?php
                        } else {
                            if ( get_field('link') ) : $file = get_field('link'); ?>
                                <a class="btn-hypno" href="<?php echo $file['url']; ?>"><?php echo $file['title']; ?></a>
                            <?php
                            endif;
                        }
                        ?>   
                    </div>
                </div>
                <!----- COL IMAGE ----->
                <?php
                    if (get_field('image_animee')) { ?>
                        <div class="col-12 col-md-12 col-lg-5 justify-content-center align-items-center d-flex block-2-columns-img">
                            <?php include(get_template_directory() . "/template-parts/blocks/svg-anim-img/svg-anim-img.php"); ?>
                        </div>
                    <?php
                    }
                ?>
                <!-- OPTION SLIDER CITATIONS  -->
                <?php
                    if (get_field('slider_citations')) { ?>
                

                        <div class="d-flex col-12 col-md-12 col-lg-6 citations-slider">

                            <?php if ( have_rows('slider_citations') ) : ?>
                                <?php while( have_rows('slider_citations') ) : the_row(); ?>
                                    <div class="block-citation-container">
                                        <div class="block-citation-content">
                                            <?php if ( get_sub_field('img') ) : $image = get_sub_field('img'); ?>
                                            
                                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                                            
                                            <?php endif; ?>
                                            <blockquote>
                                                <?php the_sub_field('citation'); ?>
                                            </blockquote>
                                            <div class="citation-signature">
                                                <?php the_sub_field('origine'); ?>
                                            </div>
                                        </div>
                                    </div>
                            
                                <?php endwhile; ?>
                            <?php endif; ?>
                            
                        </div>
                    <?php
                    }
                ?>
            </div>
        </div>
    </div>
</section>