("use strict");
(function ($, root, undefined) {
  $(function () {
    
    $('.citations-slider').slick({
        infinite: true,
        arrows: false,
        dots: true,
        adaptiveHeight: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });


    });
  })(jQuery, this);
