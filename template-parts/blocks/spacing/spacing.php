<?php

if(my_wp_is_mobile()){
    $margin = get_field('mobile');
}
else{
    $margin = get_field('desktop');
}



?>

<div <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"' ?> class="spacer-block" style="height: <?php echo $margin; ?>vh; background-color: <?php echo get_field('bkg_color'); ?>"></div>