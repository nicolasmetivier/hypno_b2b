<?php

/**
 * Image banner block template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'block-quote-' . $block['id'];

if (!empty($block['anchor'])) {
    $id = $block['anchor'];
} ?>
<div class="block-quote <?php echo array_key_exists('className', $block) ? $block['className'] : ''; ?>">
    <div class="container-fluid">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-auto">
                    <?php
                        if( get_field('quotes') ) {
                            
                            $quotes= 'with-quotes';
                        }
                    ?>
                    <div class="<?php echo $quotes; ?>">
                            <?php if( get_field('quotes') ) {?>
                                    <span class="quotebefore"></span>
                                    <span class="quoteafter"></span>
                            <?php } ?>
                            <?php the_field('block_quote'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>