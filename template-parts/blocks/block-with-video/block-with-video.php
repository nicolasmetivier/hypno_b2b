<?php

/**
 * Image banner block template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'block-with-video-' . $block['id'];

if (!empty($block['anchor'])) {
    $id = $block['anchor'];
} ?>
<section class="block-with-video <?php echo array_key_exists('className', $block) ? $block['className'] : ''; ?>" style="background: url(<?php the_field('bkg_img'); ?>); background-size: contain; background-repeat: no-repeat;">
    <div class="container-fluid">
        <div class="container">
            <div class="row block-video-row">
                <?php if ( have_rows('blocks_video') ) : ?>
                    <?php while( have_rows('blocks_video') ) : the_row(); ?>
                
                        <div class="col-lg-4 col-md-6 col-sm-8 col-12">
                            <div class="block-video-container flex-column d-flex">
                                <div class="block-video-thumbnail">
                                    <?php if ( get_sub_field('img') ) : $image = get_sub_field('img'); ?>
                                    
                                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                                    
                                    <?php endif; ?>
                                </div>
                                <div class="block-video-content">
                                    <button class="button-modal-video" id="one" att="<?php echo get_row_index(); ?>">
                                        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/svg/btn-play-video.svg' ?>" alt="button interview hypnoledge">
                                    </button>
                                    <?php the_sub_field('content'); ?>
                                </div>
                            </div>
                            
                            
                        </div>
                        
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!-- TEST MODAL -->
    <?php if ( have_rows('blocks_video') ) : ?>
        <?php while( have_rows('blocks_video') ) : the_row(); ?>
            
            <div id="modal-video-container-<?php echo get_row_index(); ?>">
                <div class="modal-video-background">
                    <div class="modal-video">
                        <div class="close-modal-video">
                            <span><?php echo __('Fermer', 'hypno_b2b'); ?></span>
                        </div>
                        <div>
                            <?php
                                // IF VIDEO OPTION IS CHECKED
                                if( get_field('choice_video') ) { ?>

                                    <video controls width="750">
                                        <?php if ( have_rows('video') ) : ?>
                                            <?php while( have_rows('video') ) : the_row(); ?>
                                            
                                                <source src="<?php the_sub_field('video_file'); ?>" type="<?php the_sub_field('video_format'); ?>">
                                            
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                        Sorry, your browser doesn't support embedded videos.
                                    </video>

                                <?php 
                                } 
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        
        <?php endwhile; ?>
    <?php endif; ?>
    <!-- TEST MODAL -->
</section>