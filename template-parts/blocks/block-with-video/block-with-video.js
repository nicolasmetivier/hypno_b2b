("use strict");
(function ($, root, undefined) {
  $(function () {
    
    // MODAL CONTACT    
      var modalContainer = ""; 

      $('.button-modal-video').click(function modal(){

        var buttonAttr = $(this).attr('att')
        var buttonId = $(this).attr('id');
        modalContainer = '#modal-video-container-' + buttonAttr;
        
        $(modalContainer).removeAttr('class').addClass(buttonId);
        $('body').addClass('modal-active');
        // START VIDEO
        setTimeout(function(){
          $(modalContainer + ' video').get(0).play(); 
        }, 1000);
      });
    
      $(document).mouseup(function (e) {

          var containerParent =  $(modalContainer);
          var container = $(modalContainer + " .modal-video");

          // if the target of the click isn't the container nor a descendant of the container
          if (!container.is(e.target) && container.has(e.target).length === 0) {
              containerParent.addClass('out');
              $('body').removeClass('modal-active');
              $(modalContainer + ' video').get(0).pause();
              $(modalContainer + ' video').get(0).currentTime = 0;
          }
      });
      var $modalClose = $('.close-modal-video');

      $($modalClose).click(function () {

          $(modalContainer).addClass('out');
          $('body').removeClass('modal-active');
          $(modalContainer + ' video').get(0).pause();
          $(modalContainer + ' video').get(0).currentTime = 0;
      });


  });
})(jQuery, this);
