("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {

      
      // IMAGE APPEAR
      $('.home-targets img').each(function (i){
        
        var that = $(this);

        let timeline = new TimelineMax();
        timeline
        .to(that, 0.5, { ease: Back.easeOut, opacity : 1, scale:'1'});
        ScrollTrigger.create({
            trigger: that,
            animation: timeline,
            start: 'top 100%',
            end: 'top 50%',
            //scrub: true,
            //markers: true
        });

    }) //END ANIM OPACITY


    var wW = $(window).width();
        if(wW > 576){
            $('.home-targets .customer-col-2').each(function (i){

              var that = $(this);
              
                        // Part 1
              gsap.to(that, { duration:0.3, scale:'1.1', 
                scrollTrigger: {
                  trigger: that,
                  start: 'top 100%',
                  end: 'top 50%',
                  scrub: true,
                  //markers: true
                } 
              });
        
            }) 
        }
      });
    });
  })(jQuery, this);
