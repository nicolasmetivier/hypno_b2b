<?php

/**
 * Image banner block template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'home-targets-' . $block['id'];

if (!empty($block['anchor'])) {
    $id = $block['anchor'];
} ?>
<section class="home-targets <?php echo array_key_exists('className', $block) ? $block['className'] : ''; ?>">
    <div class="container-fluid horizontal-scroll-768">
        <div class="container horizontal-scroll-768">
            <div class="row justify-content-md-center container-horizontal-scroll">
              <?php if ( have_rows('customers') ) : ?>
                  <?php while( have_rows('customers') ) : the_row(); ?>

                     <div class="col-10 col-sm-6 col-md-5 col-lg-5 col-xl-3 d-flex flex-column align-items-center justify-content-center customer-col customer-col-<?php echo get_row_index(); ?>">
                        <h3>
                            <?php the_sub_field('name'); ?>
                        </h3>
                        <?php if ( get_sub_field('img') ) : $image = get_sub_field('img'); ?>
                        <div class="text-center">
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                        </div>
                        <?php endif; ?>
                        <div class="customer-col-content">
                            <?php the_sub_field('content'); ?>
                        </div>
                        <?php if ( get_sub_field('lien') ) : $file = get_sub_field('lien'); ?>
                            <a class="btn-hypno" href="<?php echo $file['url']; ?>"><?php echo $file['title']; ?></a>
                        <?php endif; ?>
                    </div>
              
                  <?php endwhile; ?>
              <?php endif; ?>
            </div>
        </div>
    </div>
</section>