<?php

/**
 * Image banner block template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'quote-with-photo-' . $block['id'];

if (!empty($block['anchor'])) {
    $id = $block['anchor'];
} ?>

<section class="quote-with-photo <?php echo array_key_exists('className', $block) ? $block['className'] : ''; ?>">
    <div class="container-fluid" style="background-color: <?php the_field('bkg_color_full_width'); ?>; transform: translateY(<?php the_field('translate'); ?>%);">
        <div class="container">
            <div class="row" style="transform: translateY(-<?php the_field('translate'); ?>%);">
                <div class="col-10 col-sm-8 col-md-7 col-lg-6 col-xl-5 block-content">
                    <div>
                        <?php the_field('content'); ?>
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-10 col-lg-8 col-xl-8 block-img">
                    <div>
                      <?php if ( get_field('img') ) : $image = get_field('img'); ?>
                      
                          <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>

                      <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>