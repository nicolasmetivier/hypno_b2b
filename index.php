<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hypno-b2b
 */

get_header();
?>

	<main id="primary" class="site-main">
		<div class="container-fluid" style="background-image: url(<?php the_field('top_blog_bkg', 'option'); ?>); background-repeat: no-repeat;">
	
			<div class="container blog-ressources-container" >
				<div class="row ressources-title-row">
					<div class="col-12">
						<h1>
							<?php single_post_title(); ?>
						</h1>
						<div class="ressources-title-content">
							<?php 
							$page_for_posts_id = get_option('page_for_posts');
							echo get_post_field( 'post_content', $page_for_posts_id );
							?>
						</div>
						<div class="ressources-searchbar">
							<?php
							echo get_search_form();
							?>
							
						</div>
					</div>
				</div>

				<!-- ROW FORWARD -->
				<div class="row ressources-row-forward">
					<div class="col-12 col-md-8">
						<?php query_posts('offset=0&showposts=1'); ?>
						<?php if(have_posts()) : 
							while(have_posts()) : 
								the_post(); 
							
								get_template_part( 'template-parts/content', 'forward' );

							endwhile; 

						endif;
						
						wp_reset_query();
						?>
					</div>
					<div class="col-12 col-md-4">
						<?php
						query_posts('offset=1&showposts=2'); ?>
						<?php if(have_posts()) : ?>
	
							<h4><?php echo __('Les plus récents :','hypno_b2b'); ?></h4>
							<div class="ressources-forward-aside">

								<?php	
								while(have_posts()) : 
									the_post(); ?>
		
									<?php get_template_part( 'template-parts/content', 'side' ); ?>
		
								<?php
								endwhile; 
								?>
							</div>
						<?php
						endif; ?>
						<?php wp_reset_query(); ?>
					</div>
				</div>
				<div class="ressources-cat-filter-row w-100">
					<h4> <?php echo __('Trier par catégories :','hypno_b2b'); ?></h4>
					<div class="row">
						<?php
						$terms = get_categories();
						foreach ($terms as $term){
							$term_link = get_term_link($term);
						?>
							<div class="ressources-cat-filter">
								<a href="<?php echo $term_link ?>" data-id="<?php; echo $term->term_id; ?>" data-slug="<?php echo $term->slug; ?>">
									<h4><?php echo $term->name; ?></h4>
								</a>
							</div>
						<?php }
					?>
					<span class="ml-auto ressources-cat-all">
						<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>"><?php echo __('Tout voir','hypno_b2b'); ?></a>
					</span>
					</div>
				</div>
				<div class="row ressources-main-row" style="background-image: url(<?php the_field('bkg_blog_posts','option'); ?>);">
					<?php query_posts('offset=3'); ?>
					<?php if(have_posts()) :
						
						while(have_posts()) : 
							
							the_post();

							get_template_part( 'template-parts/content' );
						
					endwhile; 
					the_posts_navigation();
	
					else :
	
					get_template_part( 'template-parts/content', 'none' );
					endif; ?>
				</div>
				<!-- ROW TEST -->
			</div>

		</div>
	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
