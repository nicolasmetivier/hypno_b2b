<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hypno-b2b
 */

get_header();
?>

	<main id="primary" class="site-main">
		<div class="container-fluid" style="background-image: url(<?php the_field('top_blog_bkg', 'option'); ?>); background-repeat: no-repeat;">
	
			<div class="container blog-ressources-container" >
				<div class="row ressources-title-row">
					<div class="col-12">
						<div>
							<?php
								the_archive_title( '<h1 class="page-title">', '</h1>' );
								the_archive_description( '<div class="archive-description">', '</div>' );
							?>
						</div>
						<div class="ressources-title-content">
							<?php 
							$page_for_posts_id = get_option('page_for_posts');
							echo get_post_field( 'post_content', $page_for_posts_id );
							?>
						</div>
						<div class="ressources-searchbar">
							<?php
							echo get_search_form();
							?>
							
						</div>
					</div>
				</div>
				<div class="ressources-cat-filter-row">
					<h4> <?php echo __('Chercher par catégories :','hypno_b2b'); ?></h4>
					<div class="row">
						<?php
						$terms = get_categories();
						foreach ($terms as $term){
							$term_link = get_term_link($term);
						?>
							<div class="ressources-cat-filter">
								<a href="<?php echo $term_link ?>" data-id="<?php; echo $term->term_id; ?>" data-slug="<?php echo $term->slug; ?>">
									<h4><?php echo $term->name; ?></h4>
								</a>
							</div>
						<?php }
					?>
					<span class="ml-auto ressources-cat-all">
						<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>"><?php echo __('Tout voir','hypno_b2b'); ?></a>
					</span>
					</div>
				</div>
				<div class="row ressources-main-row" style="background-image: url(<?php the_field('bkg_blog_posts','option'); ?>);">				
					<?php if ( have_posts() ) :
						/* Start the Loop */
						while ( have_posts() ) :
							the_post();

							/*
							* Include the Post-Type-specific template for the content.
							* If you want to override this in a child theme, then include a file
							* called content-___.php (where ___ is the Post Type name) and that will be used instead.
							*/
							get_template_part( 'template-parts/content', get_post_type() );

						endwhile;

						the_posts_navigation();

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif;
					?>
				</div>
			</div>
		</div>
	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
