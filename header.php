<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hypno-b2b
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri() . '/assets/img/icon/apple-touch-icon.png' ?>">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri() . '/assets/img/icon/favicon-32x32.png' ?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri() . '/assets/img/icon/favicon-16x16.png' ?>">
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri() . '/assets/img/icon/site.webmanifest' ?>">
	<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri() . '/assets/img/icon/safari-pinned-tab.svg' ?>" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KB644X8');
	</script>
	<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'hypno-b2b' ); ?></a>

	<header id="masthead" class="site-header">
		<?php 
		if (wp_is_mobile()) { ?>
			<!-- HEADER MOBILE -->
			<div class="header-mobile-items-container">
				<div class="site-header-mobile-close">
					<?php include(get_stylesheet_directory() . '/assets/img/svg/close_white.svg'); ?>
				</div>
				<div>
					<div class="header-mobile-logo">
						<a href="<?php echo esc_url(get_home_url()) ?>" class="custom-logo-link">
							<?php include(get_stylesheet_directory() . '/assets/img/svg/logo-mobile.svg'); ?>
						</a>
					</div>
					<nav>
						<?php wp_nav_menu(array('theme_location' => 'menu-2')); ?>
					</nav>
					
					<div class="btn-header-sidebar">
						<?php 
						$link = get_field('book_demo', 'option');
						if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a class="btn-hypno-white" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
						<?php endif; ?>
					</div>
				</div>
				<div class="d-flex header-mobile-social">
					<!-- HEADER MOBILE SOCIAL NETWORK -->
					<?php if (have_rows('option_social_networks', 'option')): ?>
						<?php while (have_rows('option_social_networks', 'option')): the_row(); ?>
							<a class="mr-20" target="_blank" href="<?php the_sub_field('url_network', 'option'); ?>" title="<?php the_sub_field('url_network', 'option'); ?>">
								<img src="<?php echo esc_url(get_sub_field('icon_network', 'option')['url']); ?>" alt="<?php echo esc_attr(get_sub_field('option_social_networks_link', 'option')['title']); ?>">
							</a>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="container-fluid site-header-mobile">
				<div class="container">
					<div class="row justify-content-between">
						<div class="col-auto">
							<div class="site-header-mobile-toggle">
								<span></span>
								<span></span>
								<span></span>
							</div>
						</div>
						<div class="col-auto">
							<?php the_custom_logo(); ?>
						</div>
						<div class="col-auto dropdown-lang">
							<?php wp_nav_menu(array('theme_location' => 'menu-wpml')); ?>
						</div>
					</div>
				</div>
			</div>
			
		<?php
		} else { ?>
			<!-- HEADER DESKTOP -->
			<div class="container-fluid header-desktop">
				<div class="container">
					<div class="row">
						<div class="col-auto header-logo">
							<?php the_custom_logo(); ?>
						</div>
						<nav class="col-auto header-items-container d-flex align-items-center">
							<?php wp_nav_menu(array('theme_location' => 'menu-1')); ?>
						</nav>
						<div class="col-auto ml-auto d-flex position-relative">
							<?php if ( get_field('lien_prefooter', 'option') ) : $file = get_field('lien_prefooter', 'option'); ?>
								<a class="header-redirect" href="<?php echo $file['url']; ?>"><?php echo __('Connexion', 'hypno_b2b'); ?></a>
							<?php endif; ?>
							
							<div style="padding-left: 10px;">
								<?php 
								$link = get_field('book_demo', 'option');
								if( $link ): 
									$link_url = $link['url'];
									$link_title = $link['title'];
									$link_target = $link['target'] ? $link['target'] : '_self';
									?>
									<a class="btn-hypno" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
								<?php endif; ?>
							</div>

							<div class="dropdown-lang">
								<?php wp_nav_menu(array('theme_location' => 'menu-wpml')); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>
	</header><!-- #masthead -->


