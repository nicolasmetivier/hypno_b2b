<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package hypno-b2b
 */

get_header();
?>

<main role="main">
		<section>
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="container-fluid single-top-bkg" style="background: -webkit-linear-gradient(180deg, #000000 0%, rgba(0,0,0,0.65) 63%, rgba(0,0,0,0.09) 39%, rgba(0,0,0,0.42) 100%);
				 background: -o-linear-gradient(rgba(71,71,71,0.58) 0%, rgba(71,71,71,0.58) 100%);
				 background: linear-gradient(rgba(71,71,71,0.32) 0%, rgba(71,71,71,0.58) 100%),
				 url(<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>); background-repeat: no-repeat; background-size: cover; background-position: center;">
				</div>
				<div class="container-fluid" style="background-image: url(<?php the_field('top_blog_bkg', 'option'); ?>); background-repeat: no-repeat;">
					<div class="container">
						<div class="row justify-content-between">
							<div class="col-lg-8 col-12 single-post-content">
								<?php
								$categories = get_the_category();
								$category_name = $categories[0]->cat_name;
								?>
								<span class="content-post-cat"><?php echo $category_name ?></span>
								<h1>
									<?php the_title(); ?>
								</h1>
								<div class="d-flex justify-content-between row-author-post-date">
									<?php 
										// the author
										$get_author_id = get_the_author_meta('ID');
										$get_author_gravatar = get_avatar_url($get_author_id, array('size' => 25));

										echo '<div class="d-flex post-author-row"><div class="d-flex align-items-center">' . '<img src="' . $get_author_gravatar . '" alt="'. get_the_title() . '" />' . '<span class="content-post-author">' . get_the_author() . '</span></div></div>';
									?>
								<p class=""><?php the_date(); ?></p>
								</div>
								<div class="">
									<?php the_content(); ?>
								</div>
								<div class="row-share-post d-flex flex-column">
									<h3><?php echo __('Partagez l\'article','hypno_b2b'); ?></h3>
									<div>
										<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="Share this post on Facebook!" onclick="window.open(this.href); return false;"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/facebook-single.svg" alt="facebook share agence twist bordeaux"></a>
										<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo the_permalink(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/linkedin-single.svg" alt="linkedin share agence twist bordeaux"></a>
									</div>
								</div>
								<div class="row posts-pagination">
									<div class="d-flex justify-content-between w-100">
										<?php posts_nav_link(' &#183; ', 'Prec', 'Suiv'); ?>
										<span class="d-block"><?php previous_post_link(); ?></span>
										<span class="d-block"><?php next_post_link(); ?></span>
									</div>
								</div>
							</div>
							<aside class="col-lg-3 col-12">
								<div class="recent-posts pl-15 mt-50">
									<h4 class=""><?php echo __('Les plus récents','hypno_b2b'); ?></h4>
									<div>
										<?php
											query_posts('offset=0&showposts=4'); ?>
											<?php if(have_posts()) : ?>
						
												<div class="ressources-forward-aside">

													<?php	
													while(have_posts()) : 
														the_post(); ?>
							
														<?php get_template_part( 'template-parts/content', 'side' ); ?>
							
													<?php
													endwhile; 
													?>
												</div>
											<?php
											endif; ?>
										<?php wp_reset_query(); ?>
									</div>
								</div>
								<h4><?php echo __('Catégories','hypno_b2b'); ?></h4>
								<div class="d-flex tmplt-blog-category">
									<?php
									$terms = get_categories();
									foreach ($terms as $term){
										$term_link = get_term_link($term);
										?>
										<div class="ressources-cat-filter">
											<a href="<?php echo $term_link ?>" data-id="<?php; echo $term->term_id; ?>" data-slug="<?php echo $term->slug; ?>">
												<span><?php echo $term->name; ?></span>
											</a>
										</div>
									<?php }
									?>
									<span class="ressources-cat-all">
										<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>"><?php echo __('Tout voir','hypno_b2b'); ?></a>
									</span>
								</div>
								
							</aside>
						</div>
					</div>
				</div>
			</article>
			<!-- /article -->
		<?php endwhile; ?>

		<?php else: ?>
			<!-- article -->
			<article>
				<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
			</article>
			<!-- /article -->
		<?php endif; ?>
		</section>
	</main>


<?php
// get_sidebar();
get_footer();
