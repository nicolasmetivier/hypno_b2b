("use strict");
(function ($, root, undefined) {
    $(function () {

        // ANIMATION CLICK HEADER
        
        $(function () {


            // CLIK OUTSIDE HEADER 
            $(document).mouseup(function(e) 
            {
                var windowSize = $(window).width();
                var container = $(".header-mobile-items-container");
                var header = $('header.site-header');
                var triggerSubmenu = $('.trigger-submenu-mobile, .header-mobile-items-container ul li ul');
            
                // if the target of the click isn't the container nor a descendant of the container
                if (!container.is(e.target) && container.has(e.target).length === 0) 
                {
                    if (header.hasClass('mobile-open')) {
                        header.removeClass('mobile-open');
                    }
                    if (triggerSubmenu.hasClass('expand-submenu')) {
                        triggerSubmenu.removeClass('expand-submenu');
                    }
                    if (windowSize < 768) {
                        if ($('.dropdown-lang').hasClass('expand-submenu')) {
                            $('.dropdown-lang').removeClass('expand-submenu');
                        }
                    }
                }
            });

        // CLICK ON CLOSE ICON
            $('div.site-header-mobile .site-header-mobile-toggle').on('click', function () {
                $('header.site-header').addClass('mobile-open');
            });

            $('div.site-header-mobile-close').on('click', function () {
                $('header.site-header').removeClass('mobile-open');
            });
        });

        // SUBMENU MENU MOBILE
        $('.trigger-submenu-mobile').on('click', function () {
            $(this).toggleClass('expand-submenu');
            $(this).find('ul').toggleClass('expand-submenu');
        });



        var windowSize = $(window).width();
        if (windowSize >= 768) {
            // SUBMENU MENU DESKTOP
            $('.dropdown-lang').addClass('trigger-submenu');
    
            $('.trigger-submenu').hover(function () {
                $(this).toggleClass('expand-submenu');
                $(this).find('ul').toggleClass('expand-submenu');
            });
        } else if (windowSize < 768) {
            $('.dropdown-lang .wpml-ls-current-language > a').removeAttr("href");
            
            $('.dropdown-lang ').click(function() {
                $('.dropdown-lang').toggleClass('expand-submenu');
            });
                
            
        }

        // DETECT SCROLL TOP
        $(window).scroll(function (event) {
            var scroll = $(window).scrollTop();
            
            if (scroll > 0) {
                $('body').addClass('header-fixed');
            } 
            else {
                $('body').removeClass('header-fixed');
            }
        });


        // DOM READY
        $(document).ready(function () {

            $('.entry-content .is-style-container-appear-on-scroll').each(function (i){
        
                var that = $(this);
                let timeline = new TimelineMax();
                timeline
                .to(that, { ease: Power2.easeOut, opacity : 0});
                ScrollTrigger.create({
                    trigger: that,
                    animation: timeline,
                    start: 'top 5%',
                    end: 'bottom top',
                    scrub: true,
                    //markers: true
                });
        
            }) //END ANIM OPACITY
        
            $('.speed').each(function (i){
        
                var that = $(this);
                var v = that.attr('data-speed');
                var y = 10*v+"vh";
                        // Part 1
                let timeline = new TimelineMax();
                timeline
                .to(that, { ease: Power2.easeOut, y: "-="+y},'first');
                ScrollTrigger.create({
                    trigger: that,
                    animation: timeline,
                    start: 'top bottom',
                    end: 'bottom top',
                    scrub: true,
                    //markers: true
                });
        
            }) //END ANIM SPEED
    
            
        });

    


    });
})(jQuery, this);
