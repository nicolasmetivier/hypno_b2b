<?php
/**
 * hypno-b2b functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package hypno-b2b
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'hypno_b2b_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function hypno_b2b_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on hypno-b2b, use a find and replace
		 * to change 'hypno-b2b' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'hypno-b2b', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'hypno-b2b' ),
				'menu-wpml' => esc_html__( 'desktop-language', 'hypno-b2b' ),
				'menu-2' => esc_html__( 'Mobile', 'hypno-b2b' ),
			)
		);
		register_nav_menus(
			array(
				'menu-2' => esc_html__( 'Mobile', 'hypno-b2b' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'hypno_b2b_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'hypno_b2b_setup' );

/* Custom searh bar */
function custom_search_form( $form ) {
	$form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
		<div class="custom-form">
			<input type="text" placeholder=" ' . __('Rechercher ..','hypno_b2b') . '" value="' . get_search_query() . '" name="s" id="s" />
			<input type="submit" id="searchsubmit" value="" />
			<img src="' .  get_stylesheet_directory_uri() . '/assets/img/svg/search.svg' . '" alt="icon search" />
		</div>
	</form>';

	return $form;
}
add_filter( 'get_search_form', 'custom_search_form', 40 );


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function hypno_b2b_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'hypno_b2b_content_width', 640 );
}
add_action( 'after_setup_theme', 'hypno_b2b_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function hypno_b2b_widgets_init() {
	// register_sidebar(
	// 	array(
	// 		'name'          => esc_html__( 'Sidebar', 'hypno-b2b' ),
	// 		'id'            => 'sidebar-1',
	// 		'description'   => esc_html__( 'Add widgets here.', 'hypno-b2b' ),
	// 		'before_widget' => '<section id="%1$s" class="widget %2$s">',
	// 		'after_widget'  => '</section>',
	// 		'before_title'  => '<h2 class="widget-title">',
	// 		'after_title'   => '</h2>',
	// 	)
	// );
}
add_action( 'widgets_init', 'hypno_b2b_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function hypno_b2b_scripts() {
	wp_enqueue_style( 'hypno-b2b-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style('layout-style', get_stylesheet_directory_uri() . '/assets/css/layout.css', array(), '1.0.0');
	wp_style_add_data( 'hypno-b2b-style', 'rtl', 'replace' );

	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.custom.min.css', true, '1.0', 'all');
	wp_enqueue_style( 'helpers', get_stylesheet_directory_uri() . '/assets/css/helpers.css', true, '1.0', 'all');
	// wp_enqueue_style( 'header', get_stylesheet_directory_uri() . '/assets/css/header.css', true, '1.0', 'all');
	wp_enqueue_style( 'gutenberg', get_stylesheet_directory_uri() . '/assets/css/gutenberg.css', true, '1.0', 'all');
	wp_enqueue_style( 'svganim', get_stylesheet_directory_uri() . '/assets/css/svg-anim-img.css', true, '1.0', 'all');
	wp_enqueue_style( 'responsive', get_stylesheet_directory_uri() . '/assets/css/responsive.css', true, '1.0', 'all');
	//wp_enqueue_style( 'footer', get_stylesheet_directory_uri() . '/assets/css/footer.css', true, '1.0', 'all');

	wp_enqueue_script( 'hypno-b2b-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script('layout-script', get_stylesheet_directory_uri() . '/assets/js/layout.js', false, '1.0.0', 'all');
	wp_enqueue_script( 'gsap', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/gsap.min.js', ['jquery'], '1.0.0', true);
	wp_enqueue_script( 'ScrollTrigger', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/ScrollTrigger.min.js', ['jquery'], '1.0.0', true);

}
add_action( 'wp_enqueue_scripts', 'hypno_b2b_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
	
}
/*********************************/
/* Change Search Button Text
/**************************************/


/* -------------------------------------------
				GUTENBERG STYLE
--------------------------------------------- */

// Gutenberg Editor width
function gb_gutenberg_admin_styles() {
    echo '
        <style>
            /* Main column width */
            .wp-block {
                max-width: 1220px;
            }

            /* Width of "wide" blocks */
            .wp-block[data-align="wide"] {
                max-width: 1980px;
            }

            /* Width of "full-wide" blocks */
            .wp-block[data-align="full"] {
                max-width: none;
            }
        </style>
    ';
}
add_action('admin_head', 'gb_gutenberg_admin_styles');


/*********************************************************************
                            ACF Params
 ********************************************************************/


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' => __('Options'),
		'menu_title' => __('Options'),
		'menu_slug' => 'options',
		'position' => '31'
	));
}

add_action('acf/init', 'my_acf_init_block_types');
function my_acf_init_block_types() {

	/* Add gategorie for blocks */
	function my_block_category($categories, $post){
		return array_merge(
			$categories,
			array(
				array(
					'slug' => 'custom-blocks',
					'title' => __('Custom Block', 'custom-blocks'),
				),
			)
		);
	}
	add_filter('block_categories', 'my_block_category', 10, 2);

	//custom block ACF
	require 'inc/custom-blocks.php';

}

// Custom function

// CUSTOM WP_IS_MOBILE
function my_wp_is_mobile() {
    static $is_mobile;

    if ( isset($is_mobile) )
        return $is_mobile;

    if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
        $is_mobile = false;
    } elseif (
        strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false ) {
            $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') == false) {
            $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
        $is_mobile = false;
    } else {
        $is_mobile = false;
    }

    return $is_mobile;
}


function acf_img($img, $size="large", $class = null, $id = null){

	if(!empty($img)){

		if(isset($img['sizes'][$size])){
			$url = $img['sizes'][$size];
		}

		$srcset = wp_get_attachment_image_srcset($img['id'], $size);
		$sizes = wp_get_attachment_image_sizes($img['id'], $size);
		$alt = $img['alt'];

		if(function_exists('wp_get_attachment_image_srcset')){
			$img = "<img id=\"$id\" class=\"$class\" src=\"$url\" srcset=\"$srcset\" alt=\"$alt\" sizes=\"$sizes\" />";
		}

		return $img;

	}

}


/* Autoriser les fichiers SVG */
function wpc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'wpc_mime_types');





// CUSTOM GUTENBERG FONT SIZE
add_theme_support('editor-font-sizes',
array(
	array(
		'name'      => __( 'h2 with-quotes', 'socreativ-theme' ),
		'shortName' => __( 'h2-with-quotes', 'socreativ-theme' ),
		'size'      => 22,
		'slug'      => 'h2-serif'
	)
	
)
);

// BUTTON STYLE
register_block_style(
'core/button',
	array(
	'name'  => 'light-link',
	'label' => __( 'Light Link', 'wp-light-link' ),
	)
);

// GROUP
register_block_style(
'core/group',
array(
	'name'  => 'container',
	'label' => __( 'group container on grid', 'wp-group-container' ),
	)
);
register_block_style(
'core/group',
	array(
		'name'  => 'container-small',
		'label' => __( 'group container small center', 'wp-group-small-container' ),
	)
);
register_block_style(
'core/group',
	array(
		'name'  => 'container-appear-on-scroll',
		'label' => __( 'group container appear on scroll', 'wp-group-appear-container' ),
	)
);




// GUTENBERG colors
function mytheme_setup_theme_supported_features() {

	add_theme_support( 'editor-color-palette',
		array(
			array( 'name' => 'Vert 1', 'green1'  => 'green1', 'color' => '#008082' ),
			array( 'name' => 'Vert 2', 'green2'  => 'green2', 'color' => '#25A89B' ),
			array( 'name' => 'Vert 3', 'green3'  => 'green3', 'color' => '#7BC4B2' ),
			array( 'name' => 'grey', 'slug'  => 'grey', 'color' => '#f3f3f3' ),
			array( 'name' => 'grey 2', 'slug'  => 'grey2', 'color' => '#909090' ),
			array( 'name' => 'yellow', 'slug'  => 'yellow', 'color' => '#DAFF01' ),
		)
	);
}
add_action( 'after_setup_theme', 'mytheme_setup_theme_supported_features' );


